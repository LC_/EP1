#ifndef GAMEOFLIFE_HPP
#define GAMEOFLIFE_HPP

class Game_of_Life {
	private:
		char CELULA_VIVA;
		char CELULA_MORTA;
		int DIMENSAO_LINHA;
		int DIMENSAO_COLUNA;
		int Geracoes;
		int Velocidade;
	
	protected:
		char matriz_jogo[100][100];
		char grade_jogo[100][100];   

	public:
		Game_of_Life();
		void setCelulaVivaeMorta(char CELULA_VIVA, char CELULA_MORTA);
		char getCelulaViva();
		char getCelulaMorta();
		void setDimensoes(int DIMENSAO_LINHA, int DIMENSAO_COLUNA);     
		int getDimensaoLinha();
		int getDimensaoColuna(); 

		void setGeracoes(int Geracoes);
		int getGeracoes();
		
		void design_inicial();
		
		void criar_jogo();
		void roda_jogo_criado();

		void imprime_jogo();
		int vizinhos(int, int);  
        void proxima_geracao();

        void TituloMenu();

        void setVelocidade(int Velocidade);
        int getVelocidade();
        void Velocidade_Jogo();
}; 	 	
#endif