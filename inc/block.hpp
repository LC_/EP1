#ifndef BLOCK_HPP
#define BLOCK_HPP  
 
#include "game_of_life.hpp"

class Block: public Game_of_Life {

	public:
		Block();
		void design_inicial();	
		void roda_jogo();	
};
#endif