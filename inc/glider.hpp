#ifndef GLIDER_HPP
#define GLIDER_HPP  
 
#include "game_of_life.hpp"

class Glider: public Game_of_Life {

	public:
		Glider();
		void design_inicial();
		void roda_jogo();
};
#endif