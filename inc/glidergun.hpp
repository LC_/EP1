#ifndef GLIDERGUN_HPP
#define GLIDERGUN_HPP  
 
#include "game_of_life.hpp"

class GliderGun: public Game_of_Life {

	public:
		GliderGun();
		void design_inicial();
		void roda_jogo();
		
};
#endif