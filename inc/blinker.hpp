#ifndef BLINKER_HPP
#define BLINKER_HPP  
 
#include "game_of_life.hpp"

class Blinker: public Game_of_Life {

	public:
		Blinker(); 
		void design_inicial();
		void roda_jogo();
		
};
#endif