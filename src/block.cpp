#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include "block.hpp"

using namespace std;

Block::Block()
{	
}

void Block::design_inicial() {
    
    for(int i=20; i<getDimensaoLinha() + 20; i++) {
        for(int j=20; j<getDimensaoColuna() + 20; j++) {
           
            matriz_jogo[i][j]=getCelulaMorta();    
			
			matriz_jogo[21][21]=getCelulaViva();
		    matriz_jogo[21][22]=getCelulaViva();
		    matriz_jogo[22][21]=getCelulaViva();
    		matriz_jogo[22][22]=getCelulaViva();        
        }
    }
}

void Block::roda_jogo() {

    setCelulaVivaeMorta('@' , ' ');      
    setDimensoes(4, 4);

    cout<<"\n";
    design_inicial();    

    for (int i = 1; i <= 5; ++i) {                                                             
        system("clear");
        imprime_jogo();
        proxima_geracao();
        usleep(406750);         
    }
}