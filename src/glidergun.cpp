#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include "glidergun.hpp"

using namespace std;

GliderGun::GliderGun()
{	
}

void GliderGun::design_inicial() {                      // para ver regra do jogo nas bordas o desenho nao pode comecar nem terminar nos limites(0,100)
                                                        // o desenho nao pode comecar nem terminar nos limites
    for(int i=20; i<getDimensaoLinha() + 20; i++) {      // i e j nao podem comecar em 0 para vizualizar 
        for(int j=20; j<getDimensaoColuna() + 20; j++) { // e implementar as regras do jogo nas bordas
	    
	    matriz_jogo[i][j]=getCelulaMorta();

    	matriz_jogo[26][30] = getCelulaViva();              
    	matriz_jogo[27][30] = getCelulaViva();
    	matriz_jogo[26][31] = getCelulaViva();
    	matriz_jogo[27][31] = getCelulaViva();

    	matriz_jogo[26][40] = getCelulaViva();
    	matriz_jogo[27][40] = getCelulaViva();
    	matriz_jogo[28][40] = getCelulaViva();
    	matriz_jogo[25][41] = getCelulaViva();
    	matriz_jogo[29][41] = getCelulaViva();
    	matriz_jogo[24][42] = getCelulaViva();
    	matriz_jogo[30][42] = getCelulaViva();
    	matriz_jogo[24][43] = getCelulaViva();
    	matriz_jogo[30][43] = getCelulaViva();
    	matriz_jogo[27][44] = getCelulaViva();
    	matriz_jogo[25][45] = getCelulaViva();
    	matriz_jogo[29][45] = getCelulaViva();
    	matriz_jogo[26][46] = getCelulaViva();
    	matriz_jogo[27][46] = getCelulaViva();
    	matriz_jogo[28][46] = getCelulaViva();
    	matriz_jogo[27][47] = getCelulaViva();

    	matriz_jogo[24][50] = getCelulaViva();
    	matriz_jogo[25][50] = getCelulaViva();
    	matriz_jogo[26][50] = getCelulaViva();
    	matriz_jogo[24][51] = getCelulaViva();
    	matriz_jogo[25][51] = getCelulaViva();
    	matriz_jogo[26][51] = getCelulaViva();
    	matriz_jogo[23][52] = getCelulaViva();
    	matriz_jogo[27][52] = getCelulaViva();
    	matriz_jogo[22][54] = getCelulaViva();
    	matriz_jogo[23][54] = getCelulaViva();
    	matriz_jogo[27][54] = getCelulaViva();
    	matriz_jogo[28][54] = getCelulaViva();

    	matriz_jogo[24][64] = getCelulaViva();
    	matriz_jogo[25][64] = getCelulaViva();
    	matriz_jogo[24][65] = getCelulaViva();
    	matriz_jogo[25][65] = getCelulaViva();
    	}
    }
}
void GliderGun::roda_jogo() {
    int ger;
    char celula_viva;
        
    cout<<"\nDimensoes, posicoes e caractere da celula morta ja foram pre definidos...\n";   
    cout<<"\nDigite o caractere da celula viva(Ex: @, 0, o, ...):  ";
    cin>>celula_viva;
    
    setCelulaVivaeMorta(celula_viva, '.');
    setDimensoes(39 , 67); 
    
    design_inicial();     
    
    cout<<"\n";
 
    cout<<"Digite o numero de geracoes que deseja rodar: ";
    cin>>ger;
    setGeracoes(ger);
    
    cout<<"\n";
    
    Velocidade_Jogo();
    cout<<"\n";
    
    for(int i = 1; i <= getGeracoes(); ++i) {                                                             
        system("clear");
        imprime_jogo();
        proxima_geracao();   
        usleep(getVelocidade());    
    }
}