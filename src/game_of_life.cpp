#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include "game_of_life.hpp"

using namespace std;

Game_of_Life::Game_of_Life()
{
}

void Game_of_Life::setCelulaVivaeMorta(char CELULA_VIVA, char CELULA_MORTA) {
	this->CELULA_VIVA = CELULA_VIVA;
    this->CELULA_MORTA = CELULA_MORTA;
}
char Game_of_Life::getCelulaViva() {
    return CELULA_VIVA;    
}
char Game_of_Life::getCelulaMorta() {
    return CELULA_MORTA;	
}

void Game_of_Life::setDimensoes(int DIMENSAO_LINHA, int DIMENSAO_COLUNA) {
	this->DIMENSAO_LINHA = DIMENSAO_LINHA;
	this->DIMENSAO_COLUNA = DIMENSAO_COLUNA;
}
int Game_of_Life::getDimensaoLinha(){
    return DIMENSAO_LINHA;
}
int Game_of_Life::getDimensaoColuna() {
    return DIMENSAO_COLUNA;
}

void Game_of_Life::setGeracoes(int Geracoes) {
    this->Geracoes = Geracoes;
}
int Game_of_Life::getGeracoes(){
    return Geracoes;
}

void Game_of_Life::imprime_jogo() {                    // o desenho nao pode comecar nem terminar nos limites
    for(int i=20; i<getDimensaoLinha() + 20; i++) {    // i e j nao podem comecar em 0 para vizualizar      
        for(int j=20;j<getDimensaoColuna() + 20;j++) { // e implementar as regras do jogo nas bordas   
            cout<<matriz_jogo[i][j]<<" ";   
        }
        cout<<"\n";
    }
}


int Game_of_Life::vizinhos(int i, int j) {
    int count=0;

    if(matriz_jogo[i-1][j-1]==getCelulaViva()) count += 1;
    if(matriz_jogo[i-1][j]==getCelulaViva()) count += 1;
    if(matriz_jogo[i-1][j+1]==getCelulaViva()) count += 1;
    if(matriz_jogo[i][j-1]==getCelulaViva()) count += 1;
    if(matriz_jogo[i][j+1]==getCelulaViva()) count += 1;
    if(matriz_jogo[i+1][j-1]==getCelulaViva()) count += 1;
    if(matriz_jogo[i+1][j]==getCelulaViva()) count += 1;
    if(matriz_jogo[i+1][j+1]==getCelulaViva()) count += 1;

    return count ;
}

void Game_of_Life::proxima_geracao() {
    
    for(int i=0; i<100; i++) {      // para implementar a regra do jogo nas bordas do desenho elas 
        for(int j=0; j<100; j++) {  // precisam funcionar em todos os lugares da matriz 
            if(matriz_jogo[i][j]==getCelulaViva()) {
                if(vizinhos(i,j)<2) {
                    grade_jogo[i][j]=getCelulaMorta();
                }
                else if(vizinhos(i,j)>3) {   
                    grade_jogo[i][j]=getCelulaMorta();
                }
                else {
                    grade_jogo[i][j]=getCelulaViva();
                }
            }
            else {
                if(vizinhos(i,j)==3) {
                    grade_jogo[i][j]=getCelulaViva();
                }
                else {
                    grade_jogo[i][j]=getCelulaMorta();
                }
            }
        }
    }
    
    for(int i=0; i<100; i++) {      
        for(int j=0; j<100; j++){     
            matriz_jogo[i][j]=grade_jogo[i][j];
        }
    }
}

void Game_of_Life::criar_jogo(){
    char celula_viva, celula_morta;
    int linhas, colunas;

    cout<<"Escolha o caractere das celulas vivas:  ";
    cin>>celula_viva;
    cout<<"Escolha o caractere das celulas mortas: ";
    cin>>celula_morta;
    cout<<"Digite o numero de linhas:  ";
    cin>>linhas; 
    cout<<"Digite o numero de colunas: ";
    cin>>colunas;

    setCelulaVivaeMorta(celula_viva, celula_morta);      
                                   
    setDimensoes(linhas, colunas); 

    cout<<"\n";
    cout<<"Preencha "<<linhas<<" linhas e "<<colunas<<" colunas com os caracteres escolhidos das celulas vivas e mortas:\n";
    cout<<"(Recomenda-se espaco a cada celula digitada para melhor vizulizacao do desenho criado)\n";
    for(int i=20; i<getDimensaoLinha() + 20; i++) {
        for(int j=20; j<getDimensaoColuna() + 20; j++) {

           cin>>matriz_jogo[i][j];    
            
        }
    }    
}

void Game_of_Life::design_inicial() {
    criar_jogo();   
}

void Game_of_Life::roda_jogo_criado() {
    int ger;

    cout<<"\n"; 

    design_inicial();   
    
    cout<<"\nDigite o numero de geracoes: ";
    cin>>ger;
    setGeracoes(ger);
    
    cout<<"\n";
    Velocidade_Jogo();
    
    for (int i = 1; i <= getGeracoes(); ++i) {                                                             

        system("clear");
        imprime_jogo();
        proxima_geracao();
        
        usleep(getVelocidade());         
    }
}

void Game_of_Life::setVelocidade(int Velocidade) {
    this->Velocidade = Velocidade;
}

int Game_of_Life::getVelocidade(){
    return Velocidade;
}

void Game_of_Life::Velocidade_Jogo() {
    int x, velocidade = 0;
    int rapido = 40675;
    int medio = 81350;
    int lento = 406750;
    int muito_lento = 813500;

    cout<<"Escolha a velocidade que deseja rodar o jogo: \n";
    cout<< "MUITO LENTO (digite 1)\n";
    cout<< "   LENTO    (digite 2)\n";
    cout<< "   MEDIO    (digite 3)\n";
    cout<< "   RAPIDO   (digite 4)\n";  
    
    cin>>x;

    if(x==1) velocidade += muito_lento;
    if(x==2) velocidade += lento;
    if(x==3) velocidade += medio;
    if(x==4) velocidade += rapido;  
    else cout<<"Escolha um dos valores acima da proxima vez...\n";

    setVelocidade(velocidade);
}

void Game_of_Life::TituloMenu() {
    cout<<"  ____    _    __  __ _____    ___  _____   _     ___ _____ _____\n / ___|  / \\  |  \\/  | ____|  / _ \\|  ___| | |   |_ _|  ___| ____|\n| |  _  / _ \\ | |\\/| |  _|   | | | | |_    | |    | || |_  |  _|\n| |_| |/ ___ \\| |  | | |___  | |_| |  _|   | |___ | ||  _| | |___\n \\____/_/   \\_\\_|  |_|_____|  \\___/|_|     |_____|___|_|   |_____|\n\n";
} //titulo feito no figlet