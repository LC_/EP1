#include <iostream>
#include "blinker.hpp"
#include <stdlib.h>
#include <unistd.h>

using namespace std;


Blinker::Blinker()
{	
}

void Blinker::design_inicial() {     

    for(int i=20; i<getDimensaoLinha() + 20; i++) {
        for(int j=20; j<getDimensaoColuna() + 20; j++) {
           
            matriz_jogo[i][j]=getCelulaMorta();    
			
			matriz_jogo[21][22]=getCelulaViva();
		    matriz_jogo[22][22]=getCelulaViva();
		    matriz_jogo[23][22]=getCelulaViva();
        
        }
    }
}

void Blinker::roda_jogo() {
	int ger;

	setCelulaVivaeMorta('0' , ' ');      
 	setDimensoes(5, 5);

	design_inicial();    

	cout<<"\nVelocidade, posicoes, dimensoes e caracteres das celulas ja foram pre definidos...\n\n";
	cout<<"Digite o numero de geracoes que deseja rodar: ";
 	cin>>ger;
	setGeracoes(ger);

	for (int i = 1; i <= getGeracoes(); ++i) {                                                             
	    system("clear");
	    imprime_jogo();
    	proxima_geracao();
    	usleep(406750);         
   	}
}