#include <iostream>
#include <unistd.h>	
#include <stdlib.h>
#include "glider.hpp"

using namespace std;

Glider::Glider()
{	
}

void Glider::design_inicial() {     

    for(int i=20; i<getDimensaoLinha() + 20; i++) {
        for(int j=20; j<getDimensaoColuna() + 20; j++) {
           
            matriz_jogo[i][j]=getCelulaMorta();    
			
			matriz_jogo[21][22]=getCelulaViva();
		    matriz_jogo[22][23]=getCelulaViva();
		    matriz_jogo[23][21]=getCelulaViva();
		    matriz_jogo[23][22]=getCelulaViva();
		    matriz_jogo[23][23]=getCelulaViva();
        
        }
    }
}

void Glider::roda_jogo() {
    int ger;
    
    setCelulaVivaeMorta('@' , ' '); 
    setDimensoes(39 , 39); 
    
    design_inicial();     
    
    cout<<"\nVelocidade, posicoes, dimensoes e caracteres das celulas ja foram pre definidos...\n\n";
    cout<<"Digite o numero de geracoes que deseja rodar: ";
    cin>>ger;
    setGeracoes(ger);
    
    cout<<"\n";
    
    for(int i = 1; i <= getGeracoes(); ++i) {                                                             
        system("clear");
        imprime_jogo();
        proxima_geracao();   
        usleep(81350);         
    }
}