#include "game_of_life.hpp"
#include "glidergun.hpp"
#include "glider.hpp"
#include "blinker.hpp"
#include "block.hpp"

#include <iostream>
#include <unistd.h>

using namespace std;

int main(int argc, char **argv) {
	
	int x;
	Game_of_Life tm;
	
	tm.TituloMenu();

	cout<<"GLIDER GUN -> (digite 1)\n";    
	cout<<"GLIDER -----> (digite 2)\n";    //implementar regra nas bordas na criacao tb
	cout<<"BLINKER ----> (digite 3)\n";    // tente colocar todas as matrizes comecando com i,j=4 e terminando com i,j = getLinhacoluna + 4
	cout<<"BLOCK ------> (digite 4)\n";	   // depois tente colocar apenas a parte que imprime com i,j = 0 e i,j = getLinha 
	cout<<"CRIAR JOGO -> (digite 5)\n";

	cin>>x;

	if(x==1) {
		GliderGun glidergun;
		glidergun.roda_jogo();
    }
    
    else if(x==2) {
		Glider glider;
 		glider.roda_jogo();	
    }
    
    else if(x==3) {
		Blinker blinker;
 		blinker.roda_jogo();	
    }
    
    else if(x==4) {	
		Block block; 
 		block.roda_jogo();     
	}	//implementar regra das bordas, detalhes;                                          
    
    else if(x==5) {
		Game_of_Life gameoflife; 
 		gameoflife.roda_jogo_criado();
    }
    
    else
	    cout<<"Digite um dos valoes acima na proxima vez...\n";
    	cout<<"FIM!\n";
 	
 	return 0;
}